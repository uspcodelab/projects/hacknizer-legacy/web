FROM node:7.7-slim
# Docker images can start off with nothing, but it's extremely
# common to pull in from a base image. In our case we're pulling
# in from the slim version of the official Node image.
#
# Details about this image can be found here:
# https://hub.docker.com/_/node/

RUN apt-get update && apt-get install -qq -y --no-install-recommends \
      build-essential bzip2 libfontconfig
# Ensure that our apt package list is updated and install a package
# to ensure that we can extract bzip2 compressed files (bzip2) and
# run PhantomJS (libfontconfig).

LABEL com.hacknizer.maintainer="renato.cferreira@hotmail.com"
# It is good practice to set metadata for all of your Docker images.
# It's not necessary but it's a good habit.

ENV INSTALL_PATH /hacknizer
# The name of the application is hacknizer and while there
# is no standard on where your project should live inside of the
# Docker image, I like to put it in the root of the image and name
# it after the project.

RUN mkdir -p $INSTALL_PATH
# This just creates the folder in the Docker image at the
# install path we defined above.

WORKDIR $INSTALL_PATH
# We're going to be executing a number of commands below, and
# having to CD into the /hacknizer folder every time would
# be lame, so instead we can set the WORKDIR to be /hacknizer.
#
# By doing this, Docker will be smart enough to execute all
# future commands from within this directory.

COPY package.json yarn.lock ./
# This is going to copy in the package.json and yarn.lock from
# our work station at a path relative to the Dockerfile
# to the hacknizer/ path inside of the Docker image.
#
# It copies it to /hacknizer because of the WORKDIR being set.
#
# We copy in our package.json before the main app because Docker is 
# smart enough to cache "layers" when you build a Docker image.
#
# This is an advanced concept but it means that we'll be able to
# cache all of our node packages so that if we make an application
# code change, it won't re-run bundle install unless a package changed.

RUN yarn install
# Install all packages inside the image to make all dependencies
# readly available when creating a container.

COPY . .
# This might look a bit alien but it's copying in everything from
# the current directory relative to the Dockerfile, over to the
# /hacknizer folder inside of the Docker image.
#
# We can get away with using the . for the second argument because
# this is how the unix command cp (copy) works. It stands for the
# current directory.

ARG DEPLOY_ENV=prod
# Deploy environment in which the server will be run.

RUN npm run-script build -- --env=$DEPLOY_ENV
# Run webpack to transpile, minify and uglify the code. Build the code
# using variables for the environment specified above.

CMD node server.js
# This is the command that's going to be ran by default if you run the
# Docker image without any arguments.
#
# In our case, it will start the node app server while passing in
# its config file.
