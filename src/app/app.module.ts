// Angular's libraries
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

// Internal's libraries
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { HackathonListComponent } from './hackathon-list/hackathon-list.component';
import { HackathonViewComponent } from './hackathon-view/hackathon-view.component';

import { HackathonService } from './hackathon.service';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HackathonListComponent,
    HackathonViewComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule
  ],
  providers: [HackathonService],
  bootstrap: [AppComponent]
})
export class AppModule { }
