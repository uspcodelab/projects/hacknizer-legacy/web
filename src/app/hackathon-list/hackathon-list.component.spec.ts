// Angular's libraries
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';

// ReactiveX's libraries
import { Observable } from 'rxjs/Rx';

// Internal libraries
import { AppRoutingModule } from '../app-routing.module';

import { Hackathon } from '../hackathon';
import { HackathonService } from '../hackathon.service';

import { HackathonViewComponent } from '../hackathon-view/hackathon-view.component';

// Tested component
import { HackathonListComponent } from './hackathon-list.component';

/*----------------------------------------------------------------------------*/
/*                                    STUBS                                   */
/*----------------------------------------------------------------------------*/

class HackathonServiceStub {
  google_hackathon: Hackathon;
  facebook_hackathon: Hackathon;

  constructor() {
    this.google_hackathon = {
      id: '1',
      path: 'google-dev-fest-2017',
      name: 'Google dev fest',
      description: 'Come and fix nullPointerExceptions at gmail',
      start: '2017-01-11T12:00:00Z',
      end: '2017-01-12T12:00:00Z'
    };

    this.facebook_hackathon = {
      id: '2',
      path: 'facebook-hack-weekend-2017',
      name: 'Facebook hack weekend',
      description: 'We need features more annoying than video autoplay',
      start: '2017-01-11T12:00:00Z',
      end: '2017-01-12T12:00:00Z'
    };
  }

  list() {
    return Observable.of([ this.google_hackathon, this.facebook_hackathon ]);
  }

  get(id: string) {
    return Observable.of(this.google_hackathon);
  }
}

/*----------------------------------------------------------------------------*/
/*                                   TESTS                                    */
/*----------------------------------------------------------------------------*/

describe('HackathonListComponent', () => {
  let component: HackathonListComponent;
  let fixture: ComponentFixture<HackathonListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        HackathonListComponent,
        HackathonViewComponent
      ],
      providers: [
        {provide: HackathonService, useClass: HackathonServiceStub}
      ],
      imports: [AppRoutingModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HackathonListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
