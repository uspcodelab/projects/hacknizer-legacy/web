// Angular's libraries
import { Component, OnInit } from '@angular/core';

// Internal libraries
import { Hackathon } from '../hackathon';
import { HackathonService } from '../hackathon.service';

@Component({
  selector: 'app-hackathon-list',
  templateUrl: './hackathon-list.component.html',
  styleUrls: ['./hackathon-list.component.css']
})
export class HackathonListComponent implements OnInit {
  isLoading = false;
  hackathonList: Hackathon[] = [];

  constructor(private hackathonService: HackathonService) { }

  ngOnInit() {
    this.isLoading = true;
    this.hackathonService.list().subscribe(
      (hackathonList: Hackathon[]) => {
        this.hackathonList = hackathonList;
        this.isLoading = false;
        console.log('Got list: ', hackathonList);
      },
      (error) => {
        console.log('Error retrieving hackathons: ', error);
        this.isLoading = false;
      }
    );
  }
}
