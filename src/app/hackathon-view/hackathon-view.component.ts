// Angular's libraries
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

// Reactive JS extensions
import 'rxjs/add/operator/switchMap';

// Internal libraries
import { Hackathon } from '../hackathon';
import { HackathonService } from '../hackathon.service';

@Component({
  selector: 'app-hackathon-view',
  templateUrl: './hackathon-view.component.html',
  styleUrls: ['./hackathon-view.component.css']
})
export class HackathonViewComponent implements OnInit, OnDestroy {
  isLoading: boolean;
  hackathon: Hackathon = new Hackathon();

  private routeParamsSubscription: any;

  constructor(
    private route: ActivatedRoute,
    private hackathonService: HackathonService
  ) { }

  ngOnInit() {
    this.isLoading = true;
    this.routeParamsSubscription = this.route.params
      .switchMap((params: Params) => this.hackathonService.get(params['id']))
      .subscribe((hackathon: Hackathon) => {
        console.log('Got hackathon:', hackathon);
        this.hackathon = hackathon;
        this.isLoading = false;
      });
  }

  ngOnDestroy(): void {
    this.routeParamsSubscription.unsubscribe();
  }
}
