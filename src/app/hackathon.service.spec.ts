// Angular's libraries
import { TestBed, inject } from '@angular/core/testing';
import { Http, BaseRequestOptions } from '@angular/http';
import { MockBackend } from '@angular/http/testing';

// Tested service
import { HackathonService } from './hackathon.service';

describe('HackathonService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        MockBackend,
        BaseRequestOptions,
        {
          provide: Http,
          useFactory: (backendInstance: MockBackend,
                       defaultOptions: BaseRequestOptions) => {
            return new Http(backendInstance, defaultOptions);
          },
          deps: [MockBackend, BaseRequestOptions]
        },
        HackathonService
      ]
    });
  });

  it('should ...', inject([HackathonService], (service: HackathonService) => {
    expect(service).toBeTruthy();
  }));
});
