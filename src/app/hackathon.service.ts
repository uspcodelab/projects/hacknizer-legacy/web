// Angular's libraries
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

// ReactiveX's libraries
import 'rxjs/add/operator/map';

// Internal libraries
import { environment } from '../environments/environment';

import { Hackathon } from './hackathon';

@Injectable()
export class HackathonService {
  private apiUrl: string = environment.api_url;

  constructor(private http: Http) { }

  list() {
    return this.http
      .get(this.apiUrl + '/hackathons')
      .map((response: Response) => response.json() as Hackathon[]);
  }

  get(id: string) {
    return this.http
      .get(this.apiUrl + '/hackathons/' + id)
      .map((response: Response) => response.json() as Hackathon);
  }
}
