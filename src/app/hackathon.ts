export class Hackathon {
  // Fields extracted from logical model:
  // url
  // name
  // description
  // location
  // theme
  // rules
  // logo
  // start
  // end
  // membership_criteria
  // requirements

  id: String;
  path: String;
  name: String;
  description: String;
  start: String;
  end: String;
}
