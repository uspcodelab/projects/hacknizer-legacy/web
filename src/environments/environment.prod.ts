export const environment = {
  production: true,
  api_url: 'https://hacknizer-api.herokuapp.com/v1'
};
