export const environment = {
  production: true,
  api_url: 'https://staging-hacknizer-api.herokuapp.com/v1'
};
